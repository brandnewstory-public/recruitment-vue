# Zadanie rekrutacyjne Vue / js
​
Zadanie polega na napisaniu prostej responsywnej aplikacji Vue lub js.
​
### Logowanie
Pierwszy etap to utworzenie ekranu logowania na podstawie dostarczonego pliku psd `desktop_logowanie.psd`,  
endpoint to logowania:  
`POST https://admin.printilo.pl/api/recruitment/auth/login`  
dane logowania:  
login: `admin`  
password: `~i14-0/5$#4DUwG`
​
W odpowiedzi otrzymasz token potrzebny do authoryzacji.
​
### Użytkownicy
Po poprawnym zalogowaniu należy przekierować na podstronę `/users`
i pobrać listę użytkowników z endpointa  
`GET https://admin.printilo.pl/api/recruitment/users`  
wykorzystując token do autoryzacji.
​
Użytkowników należy wyświetlić w tabeli jak na `desktop_tabelka.psd`.
1) dane w tabeli można sortować (rosnąco i malejąco) poprzez kliknięcie w nagłówek danej kolumny,
2) dane w tabeli można filtrować wykorzystując formularz nad tabelą.
   ​
### Uwaga!
1) Nie możesz korzystać z żadnej biblioteki UI (Bootstrap, Tailwind, Vuetify itp).
2) Potraktuj to zadanie jako pojedynczy task z większej całości, ta aplikacja będzie rozwijana.
3) Pamiętaj o walidacji
4) Dodatkowo punktowane: dodanie testów (jednostkowych lub e2e)
5) Rozwiązanie należy przesłać jako link do repozytorium. 
